<?php
$rfaqs = $this->db->order_by(COL_TIMESTAMP,'desc')->get(TBL__FAQS)->result_array();
?>
<section class="hero-section d-flex justify-content-center align-items-center" style="background-image: url('<?=MY_IMAGEURL.'img-bg-hero.jpeg'?>')">
  <div class="section-overlay" style="opacity: 0.75 !important"></div>
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-12 mb-5 mb-lg-0">
        <div class="hero-section-text mt-5">
          <!--<h6 class="text-white">Selamat Datang</h6>-->
          <h5 class="hero-title text-white mt-4 mb-4"><?=nl2br($this->setting_web_desc)?></h5>
          <a href="<?=site_url('site/home/tracking')?>" class="custom-btn btn"><i class="far fa-search"></i>&nbsp;Tracking</a>
          <a href="<?=site_url('lapor')?>" class="custom-btn btn"><i class="far fa-exclamation-circle"></i>&nbsp;Pengaduan</a>
        </div>
      </div>
      <div class="col-lg-6 col-12">
        <div class="owl-carousel owl-theme carousel-hero">
          <?php
          foreach(glob(MY_IMAGEPATH.'slide/*') as $filename) {
            ?>
            <div class="item" style="height: 360px; background-image: url('<?=MY_IMAGEURL.'slide/'.basename($filename)?>'); background-size: cover; background-position: center; border-radius: 2%"></div>
            <?php
          }
          ?>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="about-section section-padding">
  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-12">
        <div class="about-image-wrap custom-border-radius-start">
          <img src="<?=MY_IMAGEURL.'img-bg-walikota.png'?>" class="about-image custom-border-radius-start img-fluid" alt="">
          <div class="about-info" style="border-top: 15px solid var(--custom-btn-bg-color) !important; border-radius: 15px !important; padding: 15px !important">
            <h6 class="text-white mb-0 me-2" style="font-size: 12pt">Drs. SYARMADANI, M.Si</h6>
            <p class="text-white mb-0" style="font-size: 12pt">Pj. Wali Kota Tebing Tinggi</p>
          </div>
        </div>
      </div>
      <div class="col-lg-6 col-12">
        <div class="video-thumb">
          <img src="<?=MY_IMAGEURL.'img-bg-home1.png'?>" class="about-image img-fluid" alt="">
          <div class="video-info">
            <a href="https://youtu.be/EvaxATCbMn8" target="_blank" class="youtube-icon bi-youtube"></a>
          </div>
        </div>
      </div>
      <!--<div class="col-lg-3 col-12">
        <div class="instagram-block">
          <img src="<?=MY_IMAGEURL.'img-bg-home3.png'?>" class="about-image custom-border-radius-end img-fluid" alt="">
          <div class="instagram-block-text">
            <a href="https://mpp.tebingtinggikota.go.id/" class="custom-btn btn" target="_blank">MAL PELAYANAN PUBLIK</a>
          </div>
        </div>
      </div>
    </div>-->
    <div class="col-lg-3 col-12">
      <div class="about-image-wrap custom-border-radius-end">
        <img src="<?=MY_IMAGEURL.'img-bg-kadis.png'?>" class="about-image custom-border-radius-end img-fluid" alt="">
        <div class="about-info" style="border-top: 15px solid var(--custom-btn-bg-color) !important; border-radius: 15px !important; padding: 15px !important">
          <h6 class="text-white mb-0 me-2" style="font-size: 12pt">AMRIS SIAHAAN, S.Pd, M.Si</h6>
          <p class="text-white mb-0" style="font-size: 12pt">Plt. Kepala Dinas</p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container" style="margin-top: 100px">
  <div class="row">
    <div class="col-12 mb-4">
      <h4>Mal Pelayanan Publik</h4>
      <p>Jl. Gunung Leuser, Tj. Marulak, Kec. Rambutan Kota Tebing Tinggi</p>
    </div>
    <div class="col-lg-6 col-12">
      <div class="custom-text-block custom-border-radius-start">
        <p class="text-white">
          Mal Pelayanan Publik yang hadir pertama di Sumatera Utara, memberikan layanan prima kepada masyarakat Kota Tebing Tinggi.
          Pelayanan publik yang mudah, cepat, aman, nyaman, dan terjangkau melalui Mal Pelayanan Publik (MPP) Kota Tebing Tinggi.
        </p>

        <div class="d-flex mt-4">
          <div class="counter-thumb">
            <div class="d-flex">
              <span class="counter-number" data-from="1" data-to="40" data-speed="1000">40</span>
            </div>
            <span class="counter-text text-white">Gerai OPD</span>
          </div>

          <div class="counter-thumb">
            <div class="d-flex">
              <span class="counter-number" data-from="1" data-to="129" data-speed="1000">129</span>
            </div>
            <span class="counter-text text-white">Jenis Layanan</span>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-6 col-12">
      <div class="video-thumb">
        <img src="<?=MY_IMAGEURL.'img-bg-home3.png'?>" class="about-image custom-border-radius-end img-fluid" alt="">
        <div class="video-info">
          <a href="https://youtu.be/UbKuC37UFK0" class="youtube-icon bi-youtube" target="_blank"></a>
        </div>
      </div>
    </div>
  </div>
</div>
</section>
<section class="job-section recent-jobs-section section-padding">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-lg-6 col-12 mb-4">
        <h4>Artikel / Berita Terkini</h4>
        <p>Berita terkini seputar kegiatan <?=$this->setting_org_name?>.</p>
      </div>
      <div class="clearfix"></div>
      <?php
      foreach($berita as $b) {
        $strippedcontent = strip_tags($b[COL_POSTCONTENT]);
        $tags = explode(",",$b[COL_POSTMETATAGS]);
        $img = $this->db->where(COL_ISTHUMBNAIL,1)->where(COL_POSTID, $b[COL_POSTID])->get(TBL__POSTIMAGES)->row_array();
        ?>
        <div class="col-lg-4 col-md-6 col-12">
          <div class="job-thumb job-thumb-box">
            <div
            class="job-image-box-wrap"
            style="
              height: 250px;
              width: 100%;
              background-image: url('<?=!empty($img)?MY_UPLOADURL.$img[COL_IMGPATH]:MY_IMAGEURL.'no-image.png'?>');
              background-size: cover;
              background-repeat: no-repeat;
              background-position: center;
            ">
              <div class="job-image-box-wrap-info d-flex align-items-center">
                <?php
                if(!empty($tags)) {
                  ?>
                  <p class="mb-0">
                    <?php
                    $ct = 0;
                    foreach($tags as $t) {
                      if($ct>2) break;
                      ?>
                      <span class="badge badge-level"><?=(strlen($t) > 10 ? substr(strtoupper($t), 0, 10) . "..." : strtoupper($t))?></span>
                      <?php
                      $ct++;
                    }
                    ?>
                  </p>
                  <?php
                }
                ?>
              </div>
            </div>
            <div class="job-body" style="min-height: 320px; max-height: 320px">
              <h5 class="job-title">
                <a href="<?=site_url('site/home/page/'.$b[COL_POSTSLUG])?>" class="job-title-link"><?=strlen($b[COL_POSTTITLE]) > 60 ? substr($b[COL_POSTTITLE], 0, 60) . "..." : $b[COL_POSTTITLE] ?></a>
              </h5>
              <div class="d-flex align-items-center">
                <p class="job-location"><i class="custom-icon far fa-user-circle"></i>&nbsp;&nbsp;<?=$b[COL_NAME]?></p>
                <p class="job-date"><i class="custom-icon far fa-calendar"></i>&nbsp;&nbsp;<?=date('d-m-Y', strtotime($b[COL_CREATEDON]))?></p>
              </div>
              <div class="border-top pt-3">
                <p class="job-price"><?=strlen($strippedcontent) > 150 ? substr($strippedcontent, 0, 150) . "..." : $strippedcontent ?></p>
              </div>
            </div>
          </div>
        </div>
        <?php
      }
      ?>
      <div class="col-lg-12 col-12 recent-jobs-bottom d-flex ms-auto my-4">
        <a href="<?=site_url('site/home/post/1')?>" class="custom-btn btn ms-lg-auto" style="font-size: 14pt; padding: 15px 25px">Lihat Selengkapnya <i class="far fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
</section>
<?php
if(!empty($rfaqs)) {
  ?>
  <section class="reviews-section section-padding">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-12">
          <h2 class="text-center mb-5">FAQ</h2>
          <div class="owl-carousel owl-theme reviews-carousel">
            <?php
            foreach($rfaqs as $r) {
              ?>
              <div class="reviews-thumb">
                <div class="reviews-info d-flex align-items-center">
                  <img src="<?=MY_IMAGEURL.'user.jpg'?>" class="avatar-image img-fluid" style="width: 40px; height: 40px">
                  <div class="d-flex align-items-center justify-content-between flex-wrap w-100 ms-3">
                    <p class="mb-0">
                      <strong><?=!empty($r[COL_NAME])?$r[COL_NAME]:'Publik'?></strong>
                    </p>
                  </div>
                </div>
                <div class="reviews-body" style="padding: 15px 25px 15px 40px">
                  <h6 class="reviews-title"><?=$r[COL_QUESTION]?></h6>
                  <p style="font-style: italic"><?=$r[COL_ANSWER]?></p>
                </div>
              </div>
              <?php
            }
            ?>

          </div>
        </div>
      </div>
    </div>
  </section>
  <?php
}
?>
<script type="text/javascript">
$('.carousel-hero').owlCarousel({
  loop:true,
  margin:10,
  /*nav:true,*/
  items: 1
});
</script>
