<header class="site-header" style="background-image: url('<?=MY_IMAGEURL.'img-bg-overlay2.png'?>') !important">
  <div class="section-overlay"></div>
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-12 text-center">
        <h2 class="text-white"><?=$title?></h2>
      </div>
    </div>
  </div>
</header>
<section class="job-section job-featured-section section-padding" id="job-section" style="background: var(--section-bg-color)">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-12">
        <?php
        foreach($res as $r) {
          $rfiles = $this->db
          ->where(COL_POSTID, $r[COL_POSTID])
          ->get(TBL__POSTIMAGES)
          ->result_array();
          ?>
          <div class="job-thumb d-flex" style="background: var(--white-color)">
            <div class="job-body d-flex flex-wrap flex-auto align-items-center ms-4">
              <div class="mb-3">
                <h4 class="job-title mb-lg-0">
                  <a href="job-details.html" class="job-title-link"><?=$r[COL_POSTTITLE]?></a>
                </h4>
                <div class="d-flex flex-wrap align-items-center">
                  <?php
                  foreach($rfiles as $f) {
                    if(file_exists(MY_UPLOADPATH.$f[COL_IMGPATH])) {
                      $fsize = filesize(MY_UPLOADPATH.$f[COL_IMGPATH]);
                      ?>
                      <p class="job-location mb-0">
                        <a href="<?=MY_UPLOADURL.$f[COL_IMGPATH]?>" target="_blank"><i class="custom-icon bi-download me-1"></i> Download Formulir (<?=strtoupper(human_filesize($fsize, 0))?>)</a>
                      </p>
                      <?php
                    }
                  }
                  ?>
                </div>
              </div>
              <div class="job-section-btn-wrap"><a href="<?=site_url('site/home/page/'.$r[COL_POSTSLUG])?>" class="custom-btn btn">Info Detil <i class="far fa-arrow-right"></i></a>
              </div>
            </div>
          </div>
          <?php
        }
        ?>

      </div>
    </div>
  </div>
</section>
