<?php
$rlainnya = $this->mpost->search(9,"",1);
?>
<style>
.custom-form .form-control, .custom-form .input-group {
  border-radius: var(--border-radius-small);
}
</style>
<section class="section-padding d-flex justify-content-center align-items-center">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-12">
        <form class="custom-form hero-form" id="form-tracking" action="<?=current_url()?>" method="post" role="form">
          <h3 class="text-white mb-0">Tracking</h3>
          <p class="text-white">
            <small style="font-style: italic">Silakan isi nomor permohonan yang ingin anda telusuri.</small>
          </p>
          <div class="row">
            <div class="col-lg-8 col-md-8 col-12">
              <div class="input-group">
                  <span class="input-group-text"><i class="bi-search custom-icon"></i></span>
                  <input type="text" name="trackingno" class="form-control" placeholder="Nomor Permohonan" required />
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-12">
              <button type="submit" class="form-control">Cari</button>
            </div>
          </div>
          <div class="row">
          </div>
        </form>
        <div id="tracking-res" class="contact-info d-flex align-items-center mt-3 d-none" style="background: var(--section-bg-color); border-radius: var(--border-radius-small)">
          <i class="custom-icon bi-search"></i>
          <p class="mb-2">
            <span class="contact-info-small-title">Hasil Pencarian</span>
            <span id="tracking-info"></span>
          </p>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="reviews-section section-padding">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-12">
        <h4 class="text-center mb-5">Berita / Artikel Terkini</h4>
        <div class="owl-carousel owl-theme">
          <?php
          foreach($rlainnya as $b) {
            $strippedcontent = strip_tags($b[COL_POSTCONTENT]);
            $img = $this->db->where(COL_ISTHUMBNAIL,1)->where(COL_POSTID, $b[COL_POSTID])->get(TBL__POSTIMAGES)->row_array();
            ?>
            <div class="col-12">
              <div class="job-thumb job-thumb-box bg-white">
                <div
                class="job-image-box-wrap"
                style="
                  height: 250px;
                  width: 100%;
                  background-image: url('<?=!empty($img)?MY_UPLOADURL.$img[COL_IMGPATH]:MY_IMAGEURL.'no-image.png'?>');
                  background-size: cover;
                  background-repeat: no-repeat;
                  background-position: center;
                ">
                  <div class="job-image-box-wrap-info d-flex align-items-center">
                    <p class="mb-0">
                      <span class="badge badge-level"><?=$b[COL_POSTCATEGORYNAME]?></span>
                    </p>
                  </div>
                </div>
                <div class="job-body" style="min-height: 320px; max-height: 320px">
                  <h5 class="job-title">
                    <a href="<?=site_url('site/home/page/'.$b[COL_POSTSLUG])?>" class="job-title-link"><?=strlen($b[COL_POSTTITLE]) > 60 ? substr($b[COL_POSTTITLE], 0, 60) . "..." : $b[COL_POSTTITLE] ?></a>
                  </h5>
                  <div class="d-flex align-items-center">
                    <p class="job-location"><i class="custom-icon far fa-user-circle"></i>&nbsp;&nbsp;<?=$b[COL_NAME]?></p>
                    <p class="job-date"><i class="custom-icon far fa-calendar"></i>&nbsp;&nbsp;<?=date('d-m-Y', strtotime($b[COL_CREATEDON]))?></p>
                  </div>
                  <div class="border-top pt-3">
                    <p class="job-price"><?=strlen($strippedcontent) > 150 ? substr($strippedcontent, 0, 150) . "..." : $strippedcontent ?></p>
                  </div>
                </div>
              </div>
            </div>
            <?php
          }
          ?>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript" src="<?=base_url()?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/jquery.form.js"></script>
<script type="text/javascript">
$('.owl-carousel').owlCarousel({
  loop:true,
  margin:10,
  /*nav:true,*/
  items: 3
});

$('#form-tracking').validate({
  ignore: "[type=file]",
  submitHandler: function(form) {
    var btnSubmit = $('button[type=submit]', form);
    var txtSubmit = btnSubmit.html();
    btnSubmit.html('<i class="far fa-circle-notch fa-spin"></i>');
    btnSubmit.attr('disabled', true);
    $('#tracking-res').addClass('d-none');

    /*$.getJSON("<?=GetSetting('SETTING_API_SICANTIK')?>", function(data) {
      var filtered = [];
      var search = $('[name=trackingno]', form).val();
      filtered = data.filter(function (i,n){
        return n.no_permohonan===search;
      });
      if(filtered) {
        $('#tracking-info').html(filtered[0].pesan);
      } else {
        toastr.error('Mohon maaf, data yang anda cari tidak ditemukan.');
      }
    });*/

    $(form).ajaxSubmit({
      dataType: 'json',
      type : 'post',
      success: function(res) {
        if(res.error != 0) {
          toastr.error(res.error);
        } else {
          //toastr.success(res.success);
          $('#tracking-res').removeClass('d-none');
          var dat = res.success.data;
          var search = $('[name=trackingno]', form).val();
          var filtered = [];

          filtered = dat.data.filter(function (i,n){
            return i.no_permohonan==search;
          });
          if(filtered && filtered.length > 0) {
            $('#tracking-info').html(filtered[0].pesan.replace(/, /g,'<br />').toUpperCase());
            if(filtered[0].download) {
              $('#tracking-info').append('<br />LINK DOWNLOAD : <a href="'+filtered[0].download+'" target="_blank">Klik Disini</a>');
            }
          } else {
            $('#tracking-info').html('Mohon maaf, data yang anda cari tidak ditemukan.');
            //toastr.error('Mohon maaf, data yang anda cari tidak ditemukan.');
          }
        }
      },
      error: function(data) {
        toastr.error('Mohon maaf, sedang terjadi kendala pada sistem kami. Silakan mencoba beberapa saat lagi.');
        btnSubmit.attr('disabled', false);
      },
      complete: function() {
        btnSubmit.html(txtSubmit);
        btnSubmit.attr('disabled', false);
      }
    });
    return false;
  }
});
</script>
