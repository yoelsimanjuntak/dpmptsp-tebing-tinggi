# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.38-MariaDB)
# Database: tt_dpmptsp
# Generation Time: 2023-05-29 17:24:24 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table _roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_roles`;

CREATE TABLE `_roles` (
  `RoleID` int(10) unsigned NOT NULL,
  `RoleName` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_roles` WRITE;
/*!40000 ALTER TABLE `_roles` DISABLE KEYS */;

INSERT INTO `_roles` (`RoleID`, `RoleName`)
VALUES
	(1,'Administrator'),
	(2,'Operator'),
	(3,'Publik');

/*!40000 ALTER TABLE `_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _userinformation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_userinformation`;

CREATE TABLE `_userinformation` (
  `Uniq` bigint(20) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(50) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Name` varchar(250) DEFAULT NULL,
  `IdentityNo` varchar(50) DEFAULT NULL,
  `BirthDate` date DEFAULT NULL,
  `Gender` tinyint(1) DEFAULT NULL,
  `Address` text,
  `PhoneNo` varchar(50) DEFAULT NULL,
  `ImgFile` varchar(250) DEFAULT NULL,
  `RegDate` date DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_userinformation` WRITE;
/*!40000 ALTER TABLE `_userinformation` DISABLE KEYS */;

INSERT INTO `_userinformation` (`Uniq`, `UserName`, `Email`, `Name`, `IdentityNo`, `BirthDate`, `Gender`, `Address`, `PhoneNo`, `ImgFile`, `RegDate`)
VALUES
	(1,'admin','administrator@eperizinan.tebingtinggikota.go.id','Administrator',NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-17'),
	(3,'rolassimanjuntak@gmail.com','rolassimanjuntak@gmail.com','Rolas Simanjuntak','199508122019031005',NULL,NULL,NULL,'085359867032',NULL,'2023-05-22'),
	(4,'operator','operator','Operator','-',NULL,NULL,NULL,'-',NULL,'2023-05-22'),
	(5,'rirismanik17@gmail.com','rirismanik17@gmail.com','Riris Manik','199311172020122010',NULL,NULL,NULL,'082370719569',NULL,'2023-05-22');

/*!40000 ALTER TABLE `_userinformation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_users`;

CREATE TABLE `_users` (
  `Uniq` bigint(20) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `RoleID` int(10) unsigned NOT NULL,
  `IsSuspend` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `LastLogin` datetime DEFAULT NULL,
  `LastLoginIP` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_users` WRITE;
/*!40000 ALTER TABLE `_users` DISABLE KEYS */;

INSERT INTO `_users` (`Uniq`, `UserName`, `Password`, `RoleID`, `IsSuspend`, `LastLogin`, `LastLoginIP`)
VALUES
	(1,'admin','a0326697a380d5ed7c5d43fa2d09e83f',1,0,'2023-05-09 08:15:14','::1'),
	(5,'rolassimanjuntak@gmail.com','e10adc3949ba59abbe56e057f20f883e',3,0,NULL,NULL),
	(6,'operator','e10adc3949ba59abbe56e057f20f883e',1,0,NULL,NULL),
	(7,'rirismanik17@gmail.com','e10adc3949ba59abbe56e057f20f883e',3,0,NULL,NULL);

/*!40000 ALTER TABLE `_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mcert
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mcert`;

CREATE TABLE `mcert` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IzinNama` varchar(50) DEFAULT NULL,
  `IzinRemarks1` text COMMENT 'Persyaratan',
  `IzinRemarks2` text COMMENT 'Lain-lain',
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mcert` WRITE;
/*!40000 ALTER TABLE `mcert` DISABLE KEYS */;

INSERT INTO `mcert` (`Uniq`, `IzinNama`, `IzinRemarks1`, `IzinRemarks2`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(1,'Izin Fisioterapy','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(2,'Izin Kerja Analisis','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(3,'Izin Kerja Bidan','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(4,'Izin Kerja Bidan Sementara','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(5,'Izin Kerja Kesehatan','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(6,'Izin Kerja Kesehatan Sementara','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(7,'Izin Kerja Perawat','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(8,'Izin Kerja Perawat Sementara','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(9,'Izin Kerja Tenaga Kefarmasian','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(10,'Izin Mendirikan Bangunan Tower','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(11,'Izin Operasional Rumah Sakit Kelas C dan D','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(12,'Izin Pemakaian Kekayaan Alat Berat','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(13,'Izin Pemasangan Jaringan Instalasi diatas / dibawa','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(14,'Izin Pendirian Program atau Satuan Pendidikan','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(15,'Izin Pendirian Satuan Pendidikan Anak Usia Dini (P','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(16,'Izin Penyelenggaraan Optikal','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(17,'Izin Praktek Apoteker','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(18,'Izin Praktek Apoteker test','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(19,'Izin Praktek Dokter Gigi','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(20,'Izin Praktek Dokter Hewan','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(21,'Izin Praktek Dokter Internsip','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(22,'Izin Praktek Dokter Spesialis Sementara','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(23,'Izin Praktek Dokter Spesialis test','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(24,'Izin Praktek Dokter Umum Sementara','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(25,'Izin Praktek Tenaga Kesehatan','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(26,'Izin Praktik / Kerja Fisioterapi','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(27,'Izin Praktik Ahli Teknologi Laboratorium Medik','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(28,'Izin Praktik Bidan','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(29,'Izin Reklame','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(30,'Izin Trayek Angkutan Kota','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(31,'Izin Tukang Gigi','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(32,'Izin Unit Transfusi Darah','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(33,'Komitmen Izin Koperasi Simpan Pinjam','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(34,'Komitmen Izin Lembaga Pelatihan Kerja','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(35,'Komitmen Izin Lingkungan','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(36,'Komitmen Izin Operasional Klinik','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(37,'Komitmen Izin Operasional Pengelolaan Limbah Bahan','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(38,'Komitmen Izin Operasional Rumah Sakit','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(39,'Komitmen Izin Pembuangan Air Limbah','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(40,'Komitmen Izin Pendirian Program atau Satuan Pendid','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(41,'Komitmen Izin Penyelenggaraan Angkutan Orang','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(42,'Komitmen Izin Penyelenggaraan Satuan Pendidikan No','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(43,'Komitmen Izin Usaha Industri','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(44,'Komitmen Izin Usaha Jasa Konstruksi','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(45,'Komitmen Izin Usaha Peternakan','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(46,'Komitmen Sertifikat Higiene Sanitasi Pangan','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(47,'Komitmen Sertifikat Pangan Produksi Rumah Tangga','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(48,'Komitmen Sertifikat Produksi Perusahaan Rumah Tang','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(49,'Komitmen Surat Izin Usaha Perdagangan','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(50,'Komitmen Surat Keterangan Perdagangan Minuman Bera','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(51,'Komitmen Surat Pernyataan Kesanggupan Pengelolaan ','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(52,'Komitmen Tanda Daftar Gudang','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(53,'Komitmen Tanda Daftar Usaha Pariwisata','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(54,'Pencabutan Surat Izin Praktek Apoteker','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(55,'Perubahan Email / Pencabutan NIB OSS','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(56,'Prototype Perizinan Terintegrasi','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(57,'Sertifikat Laik Higiene Sanitasi Depot Air Minum','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(58,'Sertifikat Standar Izin Apotek','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(59,'Sertifikat Standar Izin Toko Obat','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(60,'Sertifikat Standar Klinik','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(61,'Surat Keterangan Pemeriksaan Kualitas Air Laborato','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL),
	(62,'Surat Terdaftar Penyehat Tradisional','Dokumen 1;Dokumen 2;Dokumen 3;Dokumen 4;Dokumen 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','admin','2023-05-05 12:00:00',NULL,NULL);

/*!40000 ALTER TABLE `mcert` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table trequest
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trequest`;

CREATE TABLE `trequest` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `UserName` varchar(50) NOT NULL DEFAULT '',
  `IzinID` bigint(10) unsigned NOT NULL,
  `ReqRemarks` text,
  `ReqStatus` enum('DITERIMA','PROSES','SELESAI') NOT NULL DEFAULT 'DITERIMA',
  `ReqFile1` varchar(200) DEFAULT NULL,
  `ReqFile2` varchar(200) DEFAULT NULL,
  `ReqFile3` varchar(200) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `trequest` WRITE;
/*!40000 ALTER TABLE `trequest` DISABLE KEYS */;

INSERT INTO `trequest` (`Uniq`, `UserName`, `IzinID`, `ReqRemarks`, `ReqStatus`, `ReqFile1`, `ReqFile2`, `ReqFile3`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(2,'rolassimanjuntak@gmail.com',7,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','SELESAI','Bukti_pengisian_SP2020_Online.pdf',NULL,'Surat_Edaran_Kepala_LKPP_Nomor_9_Tahun_2022_2055_1.pdf','rolassimanjuntak@gmail.com','2023-05-29 22:55:03','operator','2023-05-30 00:14:35');

/*!40000 ALTER TABLE `trequest` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table trequest_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trequest_log`;

CREATE TABLE `trequest_log` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `ReqID` bigint(10) unsigned NOT NULL,
  `ReqStatus` varchar(50) NOT NULL DEFAULT '',
  `ReqRemarks` text,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `trequest_log` WRITE;
/*!40000 ALTER TABLE `trequest_log` DISABLE KEYS */;

INSERT INTO `trequest_log` (`Uniq`, `ReqID`, `ReqStatus`, `ReqRemarks`, `CreatedBy`, `CreatedOn`)
VALUES
	(2,2,'DITERIMA',NULL,'rolassimanjuntak@gmail.com','2023-05-29 22:55:03'),
	(5,2,'PROSES','Sedang diproses. Terimakasih!','operator','2023-05-30 00:03:09'),
	(6,2,'SELESAI','Silakan diunduh.','operator','2023-05-30 00:14:35');

/*!40000 ALTER TABLE `trequest_log` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
